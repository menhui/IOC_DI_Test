﻿using System;

namespace IOC_DI_Test.IRepository
{
    public interface IFristRepository<T> where T: class
    {
        T Query();
        int Add(T t);
        bool Update(T t);
        bool Delete(int id);


    }
}
