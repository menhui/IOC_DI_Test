﻿using IOC_DI_Test.IRepository;
using IOC_DI_Test.Model;
using System;

namespace _5.IOC_DI_Test.Repository
{
    public class FristRepository : IFristRepository<Frist>
    {
        public int Add(Frist t)
        {
            return 1;
        }

        public bool Delete(int id)
        {
            return true;
        }

        public Frist Query()
        {
            return new Frist() {ID= new Random().Next(99),Name= new Random().Next(99).ToString()+"-jack" };

        }

        public bool Update(Frist t)
        {
            return true;
        }
    }
}
