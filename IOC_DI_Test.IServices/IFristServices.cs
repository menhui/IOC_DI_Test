﻿using System;

namespace IOC_DI_Test.IServices
{
    public interface IFristServices<T> where T:class
    {
        T Query();
        int Add(T t);
        bool Update(T t);
        bool Delete(int id);
    }

    public interface ISingTest
    {
        int Age { get; set; }
        string Name { get; set; }
    }

    public interface ISconTest
    {
        int Age { get; set; }
        string Name { get; set; }
    }

    public interface ITranTest
    {
        int Age { get; set; }
        string Name { get; set; }
    }

    public interface IAService
    {
        void RedisTest();
    }
}
