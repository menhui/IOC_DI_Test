﻿using IOC_DI_Test.IServices;
using IOC_DI_Test.Model;
using System;

namespace IOC_DI_Test.Services
{
    public class FristServices:IFristServices<Frist>
    {
        public int Add(Frist t)
        {
            return 1;
        }

        public bool Delete(int id)
        {
            return true;
        }

        public Frist Query()
        {
            return new Frist() { ID = new Random().Next(99), Name = new Random().Next(99).ToString() + "-jack" };

        }

        public bool Update(Frist t)
        {
            return true;
        }
    }

    public class SingTest : ISingTest
    {
        public int Age { get; set; }
        public string Name { get; set; }
    }


    public class SconTest : ISconTest
    {
        public int Age { get; set; }
        public string Name { get; set; }
    }


    public class TranTest : ITranTest
    {
        public int Age { get; set; }
        public string Name { get; set; }
    }


    public class AService : IAService
    {
        ISingTest singTest;
        ISconTest sconTest;
        ITranTest tranTest;

        public AService(ISingTest sing, ITranTest tran, ISconTest scon)
        {
            this.singTest = sing;
            this.tranTest = tran;
            this.sconTest = scon;

        }
        public void RedisTest()
        {
        }
    }


}
