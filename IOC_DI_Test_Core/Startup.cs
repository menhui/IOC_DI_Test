﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using IOC_DI_Test.IServices;
using IOC_DI_Test.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace IOC_DI_Test_Core
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        //public void ConfigureServices(IServiceCollection services)
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();


            //自带注入已经成功了

            #region 自带注入
            /*
            services.AddTransient<ITranTest, TranTest>();
            services.AddScoped<ISconTest, SconTest>();
            services.AddSingleton<ISingTest, SingTest>();
            services.AddScoped<IAService, AService>();
            */
            #endregion

            #region AutoFAC
            //实例化autofac 容器
            var builder = new ContainerBuilder();

            //注册要通过反射创建的组件
            //builder.RegisterType<TranTest>().As<ITranTest>();

            //要记得!!!这个注入的是实现类层，不是接口层！不是 IServices
            var assemblysServices = Assembly.Load("IOC_DI_Test.Services"); //这里导入的前提 ：对这个项目进行引用，也就是有了耦合

            builder.RegisterAssemblyTypes(assemblysServices).AsImplementedInterfaces(); //implement 实现.  作为实现接口

            var assemblysRepository = Assembly.Load("IOC_DI_Test.Repository");//模式是 Load(解决方案名)

            builder.RegisterAssemblyTypes(assemblysRepository).AsImplementedInterfaces();

            //将services 填充 AutoFac 容器生成器
            builder.Populate(services);

            //使用已进行的组件 登记创建新容器
            var ApplicationContainer = builder.Build();
            #endregion


            return new AutofacServiceProvider(ApplicationContainer);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
